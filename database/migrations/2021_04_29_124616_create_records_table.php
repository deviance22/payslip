<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('records', function (Blueprint $table) {
            $table->id();
            $table->text('deductions'); // array of deductions and their values
            $table->text('column_names'); // array of column names
            $table->decimal('amount_earned');
            $table->decimal('total_deductions');
            $table->decimal('first_cutoff');
            $table->decimal('second_cutoff');
            $table->decimal('monthly_salary');
            $table->decimal('net_amount');
            $table->date('date_used');
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('records');
    }
}
