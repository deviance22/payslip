<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'deductions',
        'column_names',
        'amount_earned',
        'total_deductions',
        'first_cutoff',
        'second_cutoff',
        'monthly_salary',
        'net_amount',
        'date_used',
        'user_id',
        'token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'deductions' => 'array',
        'column_names' => 'array',
    ];
}
