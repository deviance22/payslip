<?php

namespace App\Http\Livewire\Payslip;

use Livewire\Component;
use App\Models\DeductionType;
use App\Models\User;
use App\Models\DeductionCategory;

class Admin extends Component
{
    public function render()
    {
        return view('livewire.payslip.admin', [
            'users'                => User::get(),
            'deduction_categories' => DeductionCategory::get(),
        ]);
    }
}
