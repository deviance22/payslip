<?php

namespace App\Http\Livewire\Payslip;

use Livewire\Component;
use App\Models\Record;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class Employee extends Component
{
    public function render(Request $request)
    {
        $where_date = $request->date_used ? date('Y-m-t', strtotime($request->date_used)) : date('Y-m-t', strtotime('today'));
        return view('livewire.payslip.employee',[
            'record' => Record::where('user_id', Auth::user()->id)->where('date_used', $where_date)->first(),
            'date_used' => $request->date_used ? date('Y-m', strtotime($request->date_used)) : date('Y-m', strtotime('today')),
            ]);
    }
}
