<?php

namespace App\Http\Livewire\Record;

use Livewire\Component;

class Edit extends Component
{
    public function render()
    {
        return view('livewire.record.edit');
    }
}
