<?php

namespace App\Http\Livewire\Record;

use Livewire\Component;

class Index extends Component
{
    public function render()
    {
        return view('livewire.record.index');
    }
}
