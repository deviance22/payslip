<?php

namespace App\Http\Livewire\Record;

use Livewire\Component;

class Add extends Component
{
    public function render()
    {
        return view('livewire.record.add');
    }
}
