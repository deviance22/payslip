<?php

namespace App\Http\Livewire\Deduction;

use Livewire\Component;

class Edit extends Component
{
    public function render()
    {
        return view('livewire.deduction.edit');
    }
}
