<?php

namespace App\Http\Livewire\User;

use Livewire\Component;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class Index extends Component
{
    public function render()
    {
        return view('livewire.user.index', ['users' => User::where('user_type', '>', '1')->get()]);
    }

    public function resetPassword($id)
    {
        dd('test');
        User::where('id', $id)->update(['password' => Hash::make('1234567890')]);
        $this->dispatchBrowserEvent('password-reset');
    }
}
