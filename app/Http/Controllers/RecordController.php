<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Redirect;
use App\Models\DeductionType;
use App\Models\Record;
use App\Models\User;
use App\Models\DeductionCategory;

class RecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('livewire.record.index', ['users' => User::get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $deductions = DeductionType::get();
        $employees = User::get();
        return view('livewire.record.add', ['employees' => $employees, 'deductions' => $deductions]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'salary'  => 'required',
            'user_id' => 'required',
            'date_used'  => 'required',
        ]);

        $column_names = $request->column;
        $deductions = [];

        foreach ($column_names as $column_name)
        {
            $camel_case_column_name = Str::camel($column_name);
            if ($request->$camel_case_column_name){
                $deductions[$column_name] = $request->$camel_case_column_name;
            } else {
                $key = array_search($column_name, $column_names);
                unset($column_names[$key]);
                $column_names = array_values($column_names);
            }
        }

        Record::create([
            'deductions'       => $deductions,
            'column_names'     => $column_names,
            'amount_earned'    => $request->amount_earned,
            'total_deductions' => $request->total_deductions,
            'first_cutoff'     => $request->first_cutoff,
            'second_cutoff'    => $request->second_cutoff,
            'monthly_salary'   => $request->salary,
            'net_amount'       => $request->net_amount,
            'date_used'        => date('Y-m-t', strtotime($request->date_used)),
            'user_id'          => $request->user_id,
        ]);
        return Redirect::route('records.index')
        ->with('success','Payslip record added succesfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $where_date = $request->date_used ? date('Y-m-t', strtotime($request->date_used)) : date('Y-m-t', strtotime('today'));
        $deduction_category_id = $request->deduction_category_id;
        $deductions = $deduction_category_id > 0 ? DeductionType::where('deduction_category_id', $deduction_category_id)->get() : DeductionType::get();
        $records    = Record::select('records.*', 'users.name')
                    // ->where('user_id', $id)
                    ->where('active', '1')
                    ->where('date_used', $where_date)
                    ->join('users', 'users.id', '=', 'records.user_id')
                    ->get();
        $deduction_types = $deductions;
        $pera_constant   = 2000;

        $grand_total_monthly_salary   = 0;
        $grand_total_pera             = count($records) * $pera_constant;
        $grand_total_amount_earned    = 0;
        $array_of_deduction_values    = [];
        $grand_total_total_deductions = 0;
        $grand_total_first_cutoff     = 0;
        $grand_total_second_cutoff    = 0;
        
        // Adding all the deduction types to an array with a default value of 0
        foreach ($deduction_types as $deduction_type)
        {
            $array_of_deduction_values += [$deduction_type->description => 0];
        }
        // Summing up all the values needed to be added
        foreach ($records as $key => $record) 
        {
            $grand_total_monthly_salary   += $record->monthly_salary;
            $grand_total_amount_earned    += $record->amount_earned;
            $grand_total_total_deductions += $record->total_deductions;
            $grand_total_first_cutoff     += $record->first_cutoff;
            $grand_total_second_cutoff    += $record->second_cutoff;
            foreach ($record->column_names as $key => $value) 
            {
                if(isset($array_of_deduction_values[$value])) {
                    $array_of_deduction_values[$value] += $record->deductions[$value];
                }
            }
        }
        $grand_total_deductions = $array_of_deduction_values;
        
        return view('livewire.record.show', [
            'records'                      => $records,
            'date_used'                    => $request->date_used ? date('Y-m', strtotime($request->date_used)) : date('Y-m', strtotime('today')),
            'deductions'                   => $deductions,
            'grand_total_monthly_salary'   => $grand_total_monthly_salary,
            'grand_total_pera'             => $grand_total_pera,
            'grand_total_amount_earned'    => $grand_total_amount_earned,
            'grand_total_deductions'       => $grand_total_deductions,
            'grand_total_total_deductions' => $grand_total_total_deductions,
            'grand_total_first_cutoff'     => $grand_total_first_cutoff,
            'grand_total_second_cutoff'    => $grand_total_second_cutoff,
            'deduction_category_id'        => $deduction_category_id,
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $records = Record::select('records.*', 'users.name')->where('records.id', $id)->join('users', 'users.id', '=', 'records.user_id')->first();
        $deductions = DeductionType::get();
        return view('livewire.record.edit', ['deductions' => $deductions, 'records' => $records]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'salary'  => 'required',
            'user_id' => 'required',
            'date_used'  => 'required',
        ]);
        $column_names = $request->column;
        $deductions = [];

        foreach ($column_names as $column_name)
        {
            $camel_case_column_name = Str::camel($column_name);
            if ($request->$camel_case_column_name){
                $deductions[$column_name] = $request->$camel_case_column_name;
            } else {
                $key = array_search($column_name, $column_names);
                unset($column_names[$key]);
                $column_names = array_values($column_names);
            }
        }

        Record::where('id', $id)->update([
            'deductions'       => $deductions,
            'column_names'     => $column_names,
            'amount_earned'    => $request->amount_earned,
            'total_deductions' => $request->total_deductions,
            'first_cutoff'     => $request->first_cutoff,
            'second_cutoff'    => $request->second_cutoff,
            'monthly_salary'   => $request->salary,
            'net_amount'       => $request->net_amount,
            'date_used'        => date('Y-m-t', strtotime($request->date_used)),
        ]);
        return Redirect::route('records.show', $request->user_id)
        ->with('success','Payslip record updated succesfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Record::where(['id' => $id])->delete();
        return Redirect::route('records.index')
        ->with('delete','Payslip record deleted succesfully!');
    }
}
