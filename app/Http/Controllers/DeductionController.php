<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DeductionType;
use App\Models\DeductionCategory;
use Illuminate\Support\Facades\Redirect;

class DeductionController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $deductions = DeductionType::get();
        $deduction_category_id = "";
        if ($request->deduction_category_id) 
        {
            $deduction_category_id = $request->deduction_category_id;
            $deductions = DeductionType::where('deduction_category_id', $deduction_category_id)->get();
        }
        return view('livewire.deduction.index', [
            'deductions' => $deductions,
            'deduction_categories' => DeductionCategory::get(),
            'deduction_category_id' => $deduction_category_id,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('livewire.deduction.add', ['deduction_categories' => DeductionCategory::get(),]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'description' => 'required',
        ]);

        DeductionType::create($request->all());
    
        return Redirect::route('deductions.index')
        ->with('success','Deduction added succesfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('livewire.deduction.edit', [
            'deduction' => DeductionType::where('id', $id)->first(),
            'deduction_categories' => DeductionCategory::get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'description' => 'required',
        ]);

        DeductionType::where('id', $id)->update(['description' => $request->description, 'deduction_category_id' => $request->deduction_category_id]);
    
        return Redirect::route('deductions.index')
        ->with('success','Deduction updated succesfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DeductionType::where(['id' => $id])->delete();
        return Redirect::route('deductions.index')
        ->with('delete','Deduction was successfully deleted');
    }
}
