<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Users') }}
        </h2>
    </x-slot>
    @if(Session::has('success'))
    <div class="max-w-sm mx-auto sm:px-6 lg:px-8 bg-green-200 mt-2 rounded-lg" role="alert">
        <div class="p-4 text-center text-green-600">
            {{ Session::get('success') }}
        </div>
    </div>
    @endif
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden sm:rounded-lg mx-auto w-full">
                <table class="table-fixed border border-black border-collapse mt-5 w-full text-center">
                    <thead>
                        <!-- <th class="border border-black w-1/12">ID</th> -->
                        <th class="border border-black w-2/4">Name</th>
                        <th class="border border-black w-2/4">E-mail</th>
                        <th class="border border-black w-2/4" colspan="2">Action</th>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                        <tr>
                            <!-- <td class="border border-black">{{ $user->id }}</td> -->
                            <td class="border border-black">{{ $user->name }}</td>
                            <td class="border border-black">{{ $user->email }}</td>
                            <td class="border-b border-black p-2">
                                <form action="{{ route('users.update', $user->id) }}" method="post">
                                    @csrf
                                    @method('PATCH')
                                    <button type="submit" class="bg-yellow-500 text-white py-2 px-4 hover:bg-yellow-700 rounded-lg">Reset Password</button>
                                </form>
                            </td>
                            <td class="border-b border-black p-2">
                                <form action="{{ route('users.update', $user->id) }}" method="post">
                                    @csrf
                                    @method('PATCH')
                                    @if ($user->active)
                                    <input type="hidden" name="active" value="0">
                                    <button type="submit" class="bg-red-500 text-white py-2 px-4 hover:bg-red-700 rounded-lg">Deactivate</button>
                                    @else
                                    <input type="hidden" name="active" value="1">
                                    <button type="submit" class="bg-green-500 text-white py-2 px-4 hover:bg-green-700 rounded-lg">Activate</button>
                                    @endif
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</x-app-layout>