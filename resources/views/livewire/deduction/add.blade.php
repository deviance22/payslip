<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Deductions') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="grid grid-cols-1 md:grid-cols-2 grid-rows-2 md:grid-rows-2">
                <form method="POST" action="{{ route('deductions.store') }}">
                    @csrf
                    <div class="">
                        <label for="description">Deduction Description</label>
                    </div>
                    <div class="mt-5">
                        <input type="text" name="description" id="description" class="rounded h-1/6">
                    </div>
                    <div class="mt-5">
                        <label for="deduction_category_id">Deduction Category</label>
                    </div>
                    <div class="mt-5">
                        <select name="deduction_category_id" id="deduction_category_id">
                            @foreach($deduction_categories as $deduction_category)
                            <option value="{{ $deduction_category->id }}">{{ $deduction_category->description }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div>
                        <button type="submit" class="bg-blue-500 text-white py-2 px-4 hover:bg-blue-700 rounded-lg mt-5">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
