<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Deductions') }}
        </h2>
    </x-slot>
    @if(Session::has('success'))
    <div class="max-w-sm mx-auto sm:px-6 lg:px-8 bg-green-200 mt-2 rounded-lg" role="alert">
        <div class="p-4 text-center text-green-600">
            {{ Session::get('success') }}
        </div>
    </div>
    @endif
    @if(Session::has('delete'))
    <div class="max-w-sm mx-auto sm:px-6 lg:px-8 bg-red-200 mt-2 rounded-lg" role="alert">
        <div class="p-4 text-center text-red-600">
            {{ Session::get('delete') }}
        </div>
    </div>
    @endif
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="mx-auto">
                <a href="{{ route('deductions.create') }}"><button class="bg-blue-500 text-white py-2 px-4 hover:bg-blue-700 rounded-lg">Add</button></a>
                <form method="GET" action="">
                    <div class="text-left">
                        <select name="deduction_category_id" id="deduction_category_id">
                            <option value="">Select Category</option>
                            @foreach($deduction_categories as $deduction_category)
                                @if ($deduction_category->id == $deduction_category_id)
                                <option value="{{ $deduction_category->id }}" selected>{{ $deduction_category->description }}</option>
                                @else
                                <option value="{{ $deduction_category->id }}">{{ $deduction_category->description }}</option>
                                @endif
                            @endforeach
                        </select>
                        <button type="submit" class="bg-blue-500 text-white py-1 px-2 hover:bg-blue-700 rounded-lg mt-5">Search</button>
                    </div>
                </form>
            </div>
            <div class="bg-white overflow-hidden sm:rounded-lg mx-auto w-full">
                <table class="table-fixed border border-black border-collapse mt-5 w-full text-center">
                    <thead>
                        <th class="border border-black w-1/12">ID</th>
                        <th class="border border-black w-2/4">Description</th>
                        <th class="border border-black w-2/4">Category</th>
                        <th class="border border-black w-2/4" colspan="2">Action</th>
                    </thead>
                    <tbody>
                        @foreach($deductions as $deduction)
                        <tr>
                            <td class="border border-black">{{ $deduction->id }}</td>
                            <td class="border border-black">{{ $deduction->description }}</td>
                            <td class="border border-black">{{ $deduction_categories[$deduction->deduction_category_id-1]->description }}</td>
                            <td class="border-b border-black p-2">
                                <a href="{{ route('deductions.edit', $deduction->id) }}"><button class="bg-yellow-500 text-white py-2 px-4 hover:bg-yellow-700 rounded-lg">Edit</button></a>
                            </td>
                            <td class="border-b border-black p-2">
                                <form action="{{ route('deductions.destroy', $deduction->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="bg-red-500 text-white py-2 px-4 hover:bg-red-700 rounded-lg" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</x-app-layout>
