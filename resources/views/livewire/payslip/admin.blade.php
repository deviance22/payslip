@if(Session::has('success'))
<div class="max-w-sm mx-auto sm:px-6 lg:px-8 bg-green-200 mt-2 rounded-lg" role="alert">
    <div class="p-4 text-center text-green-600">
        {{ Session::get('success') }}
    </div>
</div>
@endif
@if(Session::has('delete'))
<div class="max-w-sm mx-auto sm:px-6 lg:px-8 bg-red-200 mt-2 rounded-lg" role="alert">
    <div class="p-4 text-center text-red-600">
        {{ Session::get('delete') }}
    </div>
</div>
@endif
<div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="mx-auto">
            <a href="{{ route('records.create') }}"><button class="bg-blue-500 text-white py-2 px-4 hover:bg-blue-700 rounded-lg">Add Record</button></a>
            <form method="GET" action="{{ route('records.show', 1) }}">
                <div class="">
                    <select name="deduction_category_id" id="deduction_category_id">
                        <option value="0">All Categories</option>
                        @foreach($deduction_categories as $deduction_category)
                            <option value="{{ $deduction_category->id }}">{{ $deduction_category->description }}</option>
                        @endforeach
                    </select>
                    <button type="submit" class="bg-blue-500 text-white py-2 px-4 hover:bg-blue-700 rounded-lg mt-5">View Report</button>
                </div>
            </form>
        </div>
        <div class="bg-white overflow-hidden sm:rounded-lg mx-auto w-full">
            <table class="table-fixed border border-black border-collapse mt-5 w-full text-center">
                <thead>
                    <th class="border border-black w-1/12">ID</th>
                    <th class="border border-black w-1/4">Name</th>
                    <th class="border border-black w-1/4">Email</th>
                    <!-- <th class="border border-black w-2/4">Action</th> -->
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td class="border border-black">{{ $loop->iteration }}</td>
                        <td class="border border-black">{{ $user->name }}</td>
                        <td class="border border-black">{{ $user->email }}</td>
                        <!-- <td class="border-b border-black p-2">
                            <a href="{{ route('records.show', $user->id) }}"><button class="bg-blue-300 text-black py-2 px-4 hover:bg-blue-500 rounded-lg">View</button></a>
                        </td> -->
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
