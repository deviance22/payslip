<div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="mx-auto">
            <form method="GET" action="">
                <div class="text-center">
                    <input type="month" name="date_used" id="date_used" class="rounded h-1/6" placeholder="0.00"  value="{{ $date_used }}" required>
                    <button type="submit" class="bg-blue-500 text-white py-1 px-2 hover:bg-blue-700 rounded-lg mt-5">Search</button>
                </div>
            </form>
        </div>
        <div class="bg-white overflow-hidden sm:rounded-lg mx-auto w-full">
            <h2 class="text-center">DILG CAR PAYROLL AND ADVICE SLIP</h2>
            <h2 class="text-center">{{ date('F Y', strtotime($date_used)) }}</h2>
            <h2 class="text-left">Name: {{ Auth::user()->name }}</h2>
            <h2 class="text-left">Position:</h2>
            <table class="table-fixed border mt-5 w-full text-left">
                <tbody>
                    <tr>
                        <td class="w-1/12">Name:</td>
                        <td class="w-1/4">{{ Auth::user()->name }}</td>
                        <td class="w-1/4"></td>
                        <td class="text-right w-1/4"></td>
                    </tr>
                    <tr>
                        <td>Position:</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    @if ($record)
                    <tr>
                        <td></td>
                        <td>MONTHLY SALARY</td>
                        <td></td>
                        <td class="text-right">{{ number_format($record->monthly_salary, 2) }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>Add:</td>
                        <td class="text-left">PERA</td>
                        <td class="text-right">2,000.00</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>Amount Earned:</td>
                        <td></td>
                        <td class="text-right underline">{{ number_format($record->amount_earned, 2) }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>Less:</td>
                        <td></td>
                        <td></td>
                    </tr>
                    @foreach($record->deductions as $column => $deduction)
                    <tr>
                        <td></td>
                        <td></td>
                        <td>{{ $column }}</td>
                        <td class="text-right">{{ number_format($deduction, 2) }}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td></td>
                        <td>Total Deductions:</td>
                        <td></td>
                        <td class="text-right underline">{{ number_format($record->total_deductions, 2) }}</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>Net Pay:</td>
                        <td>01-15</td>
                        <td class="text-right underline">{{ number_format($record->first_cutoff, 2) }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>16-31</td>
                        <td class="text-right underline">{{ number_format($record->second_cutoff, 2) }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>Net Amount Received:</td>
                        <td></td>
                        <td class="text-right underline">{{ number_format($record->net_amount, 2) }}</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
