<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Payslip') }}
        </h2>
    </x-slot>
    <div class="bg-white overflow-x-auto sm:rounded-lg mx-auto w-full">
        <h2 class="text-center">GENERAL PAYROLL</h2>
        <h2 class="text-center">REPUBLIC OF THE PHILIPPINES</h2>
        <h2 class="text-center">DEPARTMENT OF THE INTERIOR AND LOCAL GOVERNMENT</h2>
        <h2 class="text-center">CORDILLERA ADMINISTRATIVE REGION</h2>
        <form method="GET" action="">
            <div class="text-center">
                <input type="hidden" name="deduction_category_id" value="{{ $deduction_category_id }}">
                <input type="month" name="date_used" id="date_used" class="rounded h-1/6" placeholder="0.00"  value="{{ $date_used }}" required>
                <button type="submit" class="bg-blue-500 text-white py-1 px-2 hover:bg-blue-700 rounded-lg mt-5">Search</button>
            </div>
        </form>
        <h2 class="text-right">No._____________ </h2>
        <h2 class="text-left">WE ACKNOWLEDGE receipt of cash shown opposite our names as full compensation for services rendered for the period covered.</h2>
        <table class="table-auto border border-black border-collapse mt-5 w-full text-center">
            <thead>
                <th class="border border-black w-auto">No.</th>
                <th class="border border-black w-auto">Name</th>
                <th class="border border-black w-auto">Position</th>
                <th class="border border-black w-auto">EYEE No.</th>
                <th class="border border-black w-auto">Monthly Salary</th>
                <th class="border border-black w-auto">Pera</th>
                <th class="border border-black w-auto">Amount Earned</th>
                @foreach($deductions as $deduction)
                <th class="border border-black w-auto">{{ $deduction->description }}</th>
                @endforeach
                <th class="border border-black w-auto">Total Deductions</th>
                <th class="border border-black w-auto">Net AMT Received 01-15</th>
                <th class="border border-black w-auto">Net AMT Received 16-30</th>
                <th class="border border-black w-auto" colspan="2">Actions</th>
            </thead>
            <tbody>
            @foreach ($records as $record)
                <tr>
                    <td class="border border-black">{{ $loop->iteration }}</td>
                    <td class="border border-black">{{ $record->name }}</td>
                    <td class="border border-black"></td>
                    <td class="border border-black"></td>
                    <td class="border border-black">PHP {{ number_format($record->monthly_salary, 2) }}</td>
                    <td class="border border-black">PHP 2,000.00</td>
                    <td class="border border-black">PHP {{ number_format($record->amount_earned, 2) }}</td>
                    @foreach($deductions as $deduction)
                        @if(isset($record->deductions[$deduction->description]))
                    <td class="border border-black">PHP {{ number_format($record->deductions[$deduction->description], 2) }}</td>
                        @else
                    <td class="border border-black">-</td>
                        @endif
                    @endforeach
                    <td class="border border-black">PHP {{ number_format($record->total_deductions, 2) }}</td>
                    <td class="border border-black">PHP {{ number_format($record->first_cutoff, 2) }}</td>
                    <td class="border border-black">PHP {{ number_format($record->second_cutoff, 2) }}</td>
                    <td class="border-b border-black p-2">
                        <a href="{{ route('records.edit', $record->id) }}"><button class="bg-yellow-500 text-white py-2 px-4 hover:bg-yellow-700 rounded-lg">Edit</button></a>
                    </td>
                    <td class="border-b border-black p-2">
                        <form action="{{ route('records.destroy', $record->id) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="bg-red-500 text-white py-2 px-4 hover:bg-red-700 rounded-lg" type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
                <tr>
                    <td class="border border-black"></td>
                    <td class="border border-black">GRAND-TOTAL</td>
                    <td class="border border-black"></td>
                    <td class="border border-black"></td>
                    <td class="border border-black">PHP {{ number_format($grand_total_monthly_salary, 2) }}</td>
                    <td class="border border-black">PHP {{ number_format($grand_total_pera, 2) }}</td>
                    <td class="border border-black">PHP {{ number_format($grand_total_amount_earned, 2) }}</td>
                    @foreach ($grand_total_deductions as $grand_total_deduction)
                    <td class="border border-black">PHP {{ number_format($grand_total_deduction, 2) }}</td>
                    @endforeach
                    <td class="border border-black">PHP {{ number_format($grand_total_total_deductions, 2) }}</td>
                    <td class="border border-black">PHP {{ number_format($grand_total_first_cutoff, 2) }}</td>
                    <td class="border border-black">PHP {{ number_format($grand_total_second_cutoff, 2) }}</td>
                </tr>
                <tr>
                    <td>A.</td>
                    <td colspan="6" class="text-left">CERTIFIED:  Services have been duly rendered as stated.</td>
                    <td class="border-l border-black">C.</td>
                    <td colspan="{{ count($deductions)+2 }}" class="text-left border-r border-black">CERTIFIED:  Services have been duly rendered as stated.</td>
                </tr>
                <tr>
                    <td colspan="7" class="text-center pt-20">SHIRLEY EVANGELINE V. MON</td>
                    <td colspan="{{ count($deductions)+3 }}" class=" border-l border-r border-black text-center pt-20">ARACELI A. SAN JOSE</td>
                </tr>
                <tr>
                    <td colspan="7" class="text-center border-b border-black">Authorized Official</td>
                    <td colspan="{{ count($deductions)+3 }}" class=" border-l border-r border-b border-black text-center">Head of Agency/Authorized Representative</td>
                </tr>
                <tr>
                    <td>B.</td>
                    <td colspan="6" class="text-left">CERTIFIED:  Supporting documents complete and proper, and cash available in the amount of: </td>
                    <td class="border-l border-black">D.</td>
                    <td colspan="{{ count($deductions)+2 }}" class="text-left border-r border-black">CERTIFIED: That the net amount has been deposited to each employee's ATM through Advice to Debit Account (ADA). </td>
                </tr>
                <tr>
                    <td colspan="7" class="text-center pt-20">JENNIFER S. CHAOKAS</td>
                    <td colspan="{{ count($deductions)+3 }}" class=" border-l border-r border-black text-center pt-20">MYRA G. ESNARA</td>
                </tr>
                <tr>
                    <td colspan="7" class="text-center">Head, Accounting Unit</td>
                    <td colspan="{{ count($deductions)+3 }}" class=" border-l border-r border-black text-center">Cashier</td>
                </tr>
            </tbody>
        </table>
    </div>
</x-app-layout>
