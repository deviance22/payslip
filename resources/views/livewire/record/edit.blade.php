<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Records') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="">
                <form method="POST" action="{{ route('records.update', $records->id) }}">
                    @csrf
                    @method('PATCH')
                    <div class="mt-5">
                        <h3>Employee: {{ $records->name }}</h3>
                    </div>
                    <div class="mt-5">
                        <label for="date_used">Month Cutoff*</label>
                    </div>
                    <div class="mt-5">
                        <input type="month" name="date_used" id="date_used" class="rounded h-1/6" placeholder="0.00" value="{{ date('Y-m', strtotime($records->date_used)) }}" required>
                    </div>
                    <div>
                    <div class="mt-5">
                        <label for="salary">Monthly Salary*</label>
                    </div>
                    <div class="mt-5">
                        <input type="number" name="salary" id="salary" class="rounded h-1/6" placeholder="0.00" value="{{ $records->monthly_salary }}" onblur="compute()"; step=".01" required>
                    </div>
                    <div class="mt-5">
                        <p class="font-bold" id="amount-earned">Amount Earned: PHP {{ number_format($records->amount_earned, 2) }}</p>
                    </div>
                    <div class="mt-5">
                        <p class="font-bold" id="amount-earned">Deductions:</p>
                    </div>
                    @foreach($deductions as $deduction)
                    @php
                        $fixed_identifier = Str::camel($deduction->description);
                    @endphp
                    
                    <div class="mt-5">
                        <label for="{{ $fixed_identifier }}">{{ $deduction->description }}</label>
                    </div>
                    <div class="mt-5">
                        @if(array_key_exists($deduction->description, $records->deductions))
                        <input type="number" name="{{ $fixed_identifier }}" id="{{ $fixed_identifier }}" class="rounded h-1/6 deductions" placeholder="0.00" value="{{ $records->deductions[$deduction->description] }}" onblur="compute()"; step=".01">
                        @else
                        <input type="number" name="{{ $fixed_identifier }}" id="{{ $fixed_identifier }}" class="rounded h-1/6 deductions" placeholder="0.00" onblur="compute()"; step=".01">
                        @endif
                    </div>
                    <input type="hidden" name="column[]" value="{{ $deduction->description }}">
                    @endforeach
                    <div class="mt-5">
                        <p class="font-bold" id="total-deductions">Total Deductions: PHP {{ number_format($records->total_deductions, 2) }}</p>
                    </div>
                    <div class="mt-5">
                        <p class="font-bold" id="first-cutoff">Net Pay 01-15: PHP {{ number_format($records->first_cutoff, 2) }}</p>
                    </div>
                    <div class="mt-5">
                        <p class="font-bold" id="second-cutoff">Net Pay 16-30: PHP {{ number_format($records->second_cutoff, 2) }}</p>
                    </div>
                    <div class="mt-5">
                        <p class="font-bold" id="total-received">Amount Received: PHP {{ number_format($records->net_amount, 2) }}</p>
                    </div>
                    <input type="hidden" name="amount_earned" id="amount_earned" value="{{ $records->amount_earned }}">
                    <input type="hidden" name="total_deductions" id="total_deductions" value="{{ $records->total_deductions }}">
                    <input type="hidden" name="first_cutoff" id="first_cutoff" value="{{ $records->first_cutoff }}">
                    <input type="hidden" name="second_cutoff" id="second_cutoff" value="{{ $records->second_cutoff }}">
                    <input type="hidden" name="net_amount" id="net_amount" value="{{ $records->net_amount }}">
                    <input type="hidden" name="user_id" id="user_id" value="{{ $records->user_id }}">
                    <div>
                        <!-- <button type="button" class="bg-blue-500 text-white py-2 px-4 hover:bg-blue-700 rounded-lg mt-5" onclick="compute()">Compute</button> -->
                        <button type="submit" class="bg-blue-500 text-white py-2 px-4 hover:bg-blue-700 rounded-lg mt-5">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>

<script>
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'PHP',
    });

    function compute() {
        const salary = document.getElementById('salary').value === '' ? 0 : parseFloat(document.getElementById('salary').value);
        const pera = 2000;

        document.getElementById('amount-earned').innerHTML = "Amount Earned: " + formatter.format((salary + pera));
        document.getElementById('amount_earned').value = salary + pera;

        const deductions = document.getElementsByClassName('deductions');
        let total_deductions = 0;

        for(let i = 0; i < deductions.length; i++) {
            total_deductions += deductions[i].value === '' ? 0 : parseFloat(deductions[i].value);
        }

        document.getElementById('total-deductions').innerHTML = "Total Deductions: " + formatter.format(total_deductions);
        document.getElementById('total_deductions').value = total_deductions;

        const net_pay_total = salary - total_deductions - pera;
        const first_cutoff = (net_pay_total + pera)/2 + pera;
        const second_cutoff = (net_pay_total + pera)/2;
        const total_received = net_pay_total + 4000;

        document.getElementById('first-cutoff').innerHTML = 'Net Pay 01-15: ' + formatter.format((Math.floor(first_cutoff * 100)/100).toFixed(2));
        document.getElementById('first_cutoff').value = (Math.floor(first_cutoff * 100)/100).toFixed(2);

        document.getElementById('second-cutoff').innerHTML = 'Net Pay 16-30: ' + formatter.format((Math.round(second_cutoff * 100)/100).toFixed(2));
        document.getElementById('second_cutoff').value = (Math.round(second_cutoff * 100)/100).toFixed(2);

        document.getElementById('total-received').innerHTML = 'Amount Received: ' + formatter.format(round(total_received));
        document.getElementById('net_amount').value = round(total_received);
    }

    function round(num) {
        return +(Math.round(num + "e+2")  + "e-2");
    }

    function formatCurrency(amount) {
        document.getElementById(amount.id).value = formatter.format(amount.value);
    }
</script>
