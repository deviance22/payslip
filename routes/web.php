<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DeductionController;
use App\Http\Controllers\RecordController;
use App\Http\Controllers\UserController;
use App\Http\Livewire\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//     return view('dashboard');
// })->name('dashboard');

Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

    // Route::get('/users', 'App\Http\Livewire\User\Index@render');

    Route::resources([
        'deductions' => DeductionController::class,
        'records'    => RecordController::class,
        'users'    => UserController::class,
    ]);
});

// Route::resources([
//     'deductions' => DeductionController::class,
//     'records'    => RecordController::class,
// ]);
